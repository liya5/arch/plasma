if status is-interactive
pfetch
motivate
set -g -x fish_greeting ''
starship init fish | source
end
